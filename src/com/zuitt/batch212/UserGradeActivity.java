package com.zuitt.batch212;

import java.util.Scanner;

public class UserGradeActivity {
    public static void main(String[] args) {
        Scanner appScanner = new Scanner(System.in);

        System.out.println("What is your First Name?");
        String firstName = appScanner.nextLine().trim();
        System.out.println("First Name: " + firstName);

        System.out.println("What is your Last Name?");
        String lastName = appScanner.nextLine().trim();
        System.out.println("First Name: " + lastName);

        System.out.println("First Subject Grade?");
        double firstGrade = appScanner.nextDouble();
        System.out.println("First Subject Grade: " + firstGrade);

        System.out.println("Second Subject Grade?");
        double secondGrade = appScanner.nextDouble();
        System.out.println("First Subject Grade: " + secondGrade);

        System.out.println("Third Subject Grade?");
        double thirdGrade = appScanner.nextDouble();
        System.out.println("Third Subject Grade: " + thirdGrade);

        System.out.println("Good day " + firstName + lastName);
        double aveGrade = (firstGrade + secondGrade + thirdGrade) / 3;
        System.out.println("average grade is " + aveGrade);
    }
}
