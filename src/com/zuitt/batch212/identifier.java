package com.zuitt.batch212;

public class  identifier {

    public static void main(String[] args) {
        // Identifiers act as containers for values that are to be used by the program and manipulated further as needed

    // identifier declaration
        int myNum;

    // initialization
    //literals - any constant value which can be assigned to the identifier
        myNum = 26;
        int x = 100;
        System.out.println(myNum);

    // Reassign the literal of our identifiers
        myNum = 1;
        System.out.println(myNum);

        int age;
        char middle_name;

    // constant
        final int PRINCIPAL = 1000;
        System.out.println(PRINCIPAL);
    //    PRINCIPAL = 500;

    // Primitive data types
        char letter = 'a';
        System.out.println(letter);
        boolean isMarried = false;
        System.out.println(isMarried);
        byte students = 127;
        System.out.println(students);
        short seats = 32767;
        System.out.println(seats);
        int localPopulation = 2_146_273_827;
    // underscore(_) will not affect the code
        System.out.println(localPopulation);
        long worldPopulation = 2_146_273_827L;
    // L or long literals
        System.out.println(worldPopulation);
    // decimal points
        float price = 12.99F;
        System.out.println(price);
        double temperature = 12745.43254;
    //to grab data type of an identifier, we can use getClass()
        System.out.println(((Object)temperature).getClass());
    // non primitive data types
        // String, Arrays, classes, Interface
        String name = "John Doe";
        System.out.println(name);
        String editedName = name.toLowerCase();
        System.out.println(editedName);
        System.out.println(name.getClass());
    // Escape characters (\n, \")
        System.out.println("c:\\windows\\desktop");
    // typeCasting
        // Implicit Casting/Widening Type Casting - when the conversion automatically performs by the compiler without the programmer's interference, triggered if the conversion involves a smaller data type to the larger type
            // byte -> short -> int -> long -> float -> double
        //convert byte(smaller) to double(larger)
        byte num1 = 5;
        double num2 = 2.7;
        double total = num1 + num2;
        System.out.println(total);

        // Explicit Casting/Narrowing type casting - we manually convert one data type into another using the parenthesis
        //converting double into an int
        int num3 = 6;
        double num4 = 7.4;
        int anotherTotal = (int) (num3 + num4);
        System.out.println(anotherTotal);

        //Concatenation
        String mathGrade = "98";
        String englishGrade = "85";
        System.out.println(mathGrade + englishGrade);

        // Converting strings into integers
        int totalGrade = Integer.parseInt(mathGrade) + Integer.parseInt(englishGrade);
        System.out.println(totalGrade);

        //Converting integers
        String stringGrade = Integer.toString(totalGrade);
        System.out.println(stringGrade.getClass());
    }

}
